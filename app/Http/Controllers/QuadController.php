<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Quad;
use Facade\FlareClient\View;
use Illuminate\Http\Request;

class QuadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quads=Quad::with('category')->get();
        
        
        return view('quad.indexquad',compact('quads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorie=Category::all();
        return view('quad.creaQuad',compact('categorie'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* dd($request->all()); */
        $quad=Quad::create([
            'nome'=>$request->nome,
            'posti'=>$request->posti,
            'manutenzione'=>$request->manutenzione,
            'bloccato'=>$request->bloccato,
            'category_id'=>$request->categoria
        ]);

        return redirect()->back()->with('message','Quad inserito');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quad  $quad
     * @return \Illuminate\Http\Response
     */
    public function show(Quad $quad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quad  $quad
     * @return \Illuminate\Http\Response
     */
    public function edit(Quad $quad)
    {
        /* $quad=Quad::where('id',$quad->id)->get();
        dd($quad); */
        $categorie=Category::all();
        //$categoriaQuad=Category::where('id',$quad->id_categoria)->get();
        return view('quad.edit',compact('quad','categorie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quad  $quad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quad $quad)
    {
        $quad->nome=$request->nome;
        $quad->posti=$request->posti;
        $quad->manutenzione=$request->manutenzione;
        $quad->bloccato=$request->bloccato;
        $quad->category_id=$request->categoria;
        $quad->save();

        return redirect()->back()->with('message','Quad modificato con successo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quad  $quad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quad $quad)
    {
        //
    }
}
