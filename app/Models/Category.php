<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable=[
        'id',
        'nome',
        'prezzo',
        'cover_image',
        'caratteristiche',
        'attivo'
    ];

    public function quad(){
        return $this->hasMany(Quad::class,'id_categoria');
    }
}
