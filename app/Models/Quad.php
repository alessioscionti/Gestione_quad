<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quad extends Model
{
    use HasFactory;

    protected $fillable=[
        'nome',
        'posti',
        'manutenzione',
        'bloccato',
        'category_id'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
