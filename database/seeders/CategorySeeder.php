<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            "Test 1"=> [
                "nome"=>"Test 1",
                "prezzo"=>100,
                "cover_image"=>null,
                "caratteristiche"=>null,
                "attivo"=>1
            ],
            "Test 2"=>[
                "nome"=>"Test 2",
                "prezzo"=>120,
                "cover_image"=>null,
                "caratteristiche"=>null,
                "attivo"=>1 
            ],
            "Test 3"=>[
                "nome"=>"Test 3",
                "prezzo"=>150,
                "cover_image"=>null,
                "caratteristiche"=>null,
                "attivo"=>1 
            ]
        ];

        foreach ($category as $key => $value) {
            $newCategory = new Category;
            $newCategory->nome=$key;
            $newCategory->prezzo=$value['prezzo'];
            $newCategory->cover_image=$value['cover_image'];
            $newCategory->caratteristiche=$value['caratteristiche'];
            $newCategory->attivo=$value['attivo'];



            $newCategory->save();
        }
    }
}
