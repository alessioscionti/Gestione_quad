<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Quad;
use Illuminate\Database\Seeder;

class QuadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        
        for ($i=0; $i <10 ; $i++) { 
            $getCategory = Category::inRandomOrder()->first();

            $newQuad= new Quad;
            $newQuad->nome= $faker->name();
            $newQuad->posti=2;
            $newQuad->manutenzione= $faker->boolean();
            $newQuad->bloccato=$faker->boolean();
            $newQuad->category_id= $getCategory->id;

            $newQuad->save();
        }
    }
}
