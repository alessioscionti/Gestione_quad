<x-layout>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>Gestione Categorie</h1>
            </div>
        </div>
    </div>
    @if (Session::has('message'))
        <div class="alert alert-success text-center">
            {{session('message')}}
        </div>
    @endif
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <form action="{{route('categorie.store')}}" method="post">
                    @csrf
                    <div class="form-group row mt-3">
                        <label for="nome" class="col-sm-2 col-form-label">Nome Categoria</label>
                        <div class="col-sm-10">
                            <input type="text" name="nome" value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="prezzo" class="col-sm-2 col-form-label">Prezzo Categoria</label>
                        <div class="col-sm-10">
                            <input type="text" name="prezzo" value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="cover_image" class="col-sm-2 col-form-label">Immagine Categoria</label>
                        <div class="col-sm-10">
                            <input type="text" name="cover_image" value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="caratteristiche" class="col-sm-2 col-form-label">caratteristiche Categoria</label>
                        <div class="col-sm-10">
                            <input type="text" name="caretteristiche" value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="attivo" class="col-sm-2 col-form-label">Categoria subito attiva?</label>
                        <div class="col-sm-10">
                            <select name="attivo" id="attivo">
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Salva</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>