<nav id="nav" class="nav">
  <div id="nav_icon" class="nav_icon">
      <svg class="shape_container top">
          <path class="icon_shape" d="M 0 0 L 20 0 Q 0 0 0 20 Z" />
      </svg>
      <span>MENU</span><i class="fas fa-angle-right"></i>
      <svg class="shape_container bottom">
          <path class="icon_shape" d="M 0 0 L 20 0 Q 0 0 0 20 Z" />
      </svg>
  </div>
  <div>
    @if (Auth::User())
        
        
    <p>Benvenuto <span style="color:white;">{{Auth::User()->name}}</span></p>
    
    @endif
  </div>
  <ul>
      <li>
          <a href="{{route('home')}}"><i class="fas fa-home"></i>Home</a>
      </li>
      <li>
          <a href="#sa"><i class="fas fa-hat-cowboy-side"></i>Prenotazioni</a>
      </li>
      <li>
          <a href="{{route('indexquad')}}"><i class="far fa-gem"></i>Gestione Quad</a>
      </li>
      <li>
        <a href="{{route('creacat')}}"><i class="far fa-gem"></i>Gestione Categorie Quad</a>
    </li>
      @guest
      <li>
        <a href="{{route('login')}}"><i class="fas fa-globe-europe"></i>Login</a>
      </li>
      <li>
          <a href="{{route('register')}}"><i class="fas fa-network-wired"></i>Register</a>
      </li>
      @else
      <li>
      <a href="{{route('logout')}}" class="nav-link p-2" onclick="event.preventDefault();document.getElementById('logout').submit();" style="margin-top:-1rem;">
        <i class="icon ri-shut-down-line"></i>
        <span class="link-text">Logout</span>
     </a>
    </li>
     <form method="POST" action="{{route('logout')}}" id="logout">
       @csrf
     </form>
      @endguest
  </ul>
</nav>