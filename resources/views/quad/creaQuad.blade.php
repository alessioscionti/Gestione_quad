<x-layout>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>Inserisci nuovo Quad</h1>
            </div>
        </div>
    </div>
    @if (Session::has('message'))
        <div class="alert alert-success text-center">
            {{session('message')}}
        </div>
    @endif
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <form action="{{route('quad.store')}}" method="post">
                    @csrf
                    <div class="form-group row mt-3">
                        <label for="nome" class="col-sm-2 col-form-label">Nome Quad</label>
                        <div class="col-sm-10">
                            <input type="text" name="nome" value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="posti" class="col-sm-2 col-form-label">Quanti posti?</label>
                        <div class="col-sm-10">
                            <input type="text" name="posti" value="" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="manutenzione" class="col-sm-2 col-form-label">il mezzo è in manutenzione?</label>
                        <div class="col-sm-10">
                            <select name="manutenzione" id="attivo">
                                <option value=""selected>--</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="manutenzione" class="col-sm-2 col-form-label">vuoi bloccare subito il mezzo?</label>
                        <div class="col-sm-10">
                            <select name="bloccato" id="attivo">
                                <option value=""selected>--</option>
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="categoria" class="col-sm-2 col-form-label">Categoria Quad</label>
                        <div class="col-sm-10">
                            <select name="categoria" id="attivo">
                                <option value=""selected>--</option>
                                @foreach ($categorie as $categoria)
                                <option value="{{$categoria->id}}">{{$categoria->nome}}</option> 
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success">Salva</button>
                </form>
            </div>
        </div>
    </div>
</x-layout>