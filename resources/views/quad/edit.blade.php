<x-layout>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>Modifica {{$quad->nome}}</h1>
            </div>
        
            <div class="col-1 text-center">
                <div class="photo-container">
                    <svg viewBox="0 0 220 220">
                      <circle shape-rendering="geometricPrecision"class="indicator"cx="110"cy="110"r="96"/>
                    </svg>
                    <div class="img-box text-center">
                        <img class="img-card" src="https://can-am.brp.com/content/can-am-off-road/it_it/modello/quad/outlander-450-570/_jcr_content/root/modelteaser.coreimg.png/1667415435897/orv-atv-my23-can-am-outlander-max-xt-650dt-oxford-blue-0002zpc00-34fr-t3abs.png" alt="" />
                    </div>
                  </div>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                <form action="{{route('quad.update',$quad)}}" method="post">
                    @csrf
                    <div class="form-group row mt-3">
                        <label for="nome" class="col-sm-2 col-form-label">Nome Quad</label>
                        <div class="col-sm-10">
                            <input type="text" name="nome" value="{{$quad->nome}}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="posti" class="col-sm-2 col-form-label">Quanti posti?</label>
                        <div class="col-sm-10">
                            <input type="text" name="posti" value="{{$quad->posti}}" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="manutenzione" class="col-sm-2 col-form-label">il mezzo è in manutenzione?</label>
                        <div class="col-sm-10">
                            <select name="manutenzione" id="attivo">
                                @if ($quad->manutenzione==1)
                                <option value="{{$quad->manutenzione}}" selected>➤ <span style="color:green;">SI</span></option>
                                @else
                                <option value="{{$quad->manutenzione}}" selected>➤ <span style="color:green;">NO</span></option>
                                @endif
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="bloccato" class="col-sm-2 col-form-label">vuoi bloccare il mezzo?</label>
                        <div class="col-sm-10">
                            <select name="bloccato" id="attivo">
                                @if ($quad->bloccato==1)
                                <option value="{{$quad->manutenzione}}" selected>➤ <span style="color:green;">SI</span></option>
                                @else
                                <option value="{{$quad->manutenzione}}" selected>➤ <span style="color:green;">NO</span></option>
                                @endif
                                <option value="1">Si</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <label for="categoria" class="col-sm-2 col-form-label">Categoria Quad</label>
                        <div class="col-sm-10">
                            <select name="categoria" id="attivo">
                                <option value="{{$quad->id_categoria}}"selected>➤ {{$quad->category->nome}}</option>
                                @foreach ($categorie as $categoria)
                                <option value="{{$categoria->id}}">{{$categoria->nome}}</option> 
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-success text-center">Salva</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-layout>