<x-layout>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1>Gestione Quad</h1>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-6 text-center">
                <button class="btn btn-danger"><a href="{{route('creaquad')}}"> Inserisci Nuovo Quad</a></button>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            @foreach ($quads as $quad)
            {{-- <form action="{{route('quad.edit','quad')}}" method="post"> --}}
            @csrf
                <div class="col-4 mt-3">
                    <div class="card">
                        <div class="photo-container">
                          <svg viewBox="0 0 220 220">
                            <circle shape-rendering="geometricPrecision"class="indicator"cx="110"cy="110"r="96"/>
                          </svg>
                          <div class="img-box">
                              <img class="img-card" src="https://can-am.brp.com/content/can-am-off-road/it_it/modello/quad/outlander-450-570/_jcr_content/root/modelteaser.coreimg.png/1667415435897/orv-atv-my23-can-am-outlander-max-xt-650dt-oxford-blue-0002zpc00-34fr-t3abs.png" alt="" />
                          </div>
                        </div>
                        <h3 class="h3-card">{{$quad->nome}}</h3>
                        <span class="span-card">{{$quad->category->nome}}</span>
                        <div class="box-container">
                          <div class="box">
                            <i class="fa-solid fa-euro-sign"></i>
                            <span>{{$quad->category->prezzo}}</span>
                          </div>
                          <div class="box">
                            <i class="fas fa-users"></i>
                            <span>{{$quad->posti}}</span>
                          </div>
                        </div>
                        <button type="submit" class="btn btn-danger"><a href="{{route('quad.edit',$quad)}}">Modifica</a></button>
                      </div>
                </div>
            {{-- </form> --}}
            @endforeach
        </div>
    </div>
</x-layout>