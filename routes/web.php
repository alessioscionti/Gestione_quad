<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\QuadController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/quad', [QuadController::class, 'index'])->name('indexquad');
Route::get('/creaquad', [QuadController::class, 'create'])->name('creaquad');
Route::post('/creaquad/store', [QuadController::class, 'store'])->name('quad.store');
Route::get('/creaquad/edit/{quad}', [QuadController::class, 'edit'])->name('quad.edit');
Route::post('/creaquad/update/{quad}', [QuadController::class, 'update'])->name('quad.update');
Route::get('/creacat',[CategoryController::class,'create'])->name('creacat');
Route::post('/category/store', [CategoryController::class, 'store'])->name('categorie.store');

